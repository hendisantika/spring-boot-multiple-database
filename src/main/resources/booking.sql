CREATE TABLE booking(id BIGINT NOT NULL AUTO_INCREMENT, bookingAmount VARCHAR(255), createdBy BIGINT, dropAddress VARCHAR(255), pickupAddress  VARCHAR(255), PRIMARY KEY (id)) ENGINE=InnoDB;

INSERT INTO booking(bookingAmount,createdBy,dropAddress,pickupAddress)VALUES('212',1,'Cimahi','Jawa Barat');

INSERT INTO booking(bookingAmount,createdBy,dropAddress,pickupAddress)VALUES('414',1,'Bandung','Jakarta');
package com.hendisantika.user.dao;

import com.hendisantika.model.UserDetails;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-database
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 06.38
 * To change this template use File | Settings | File Templates.
 */


public interface UserDao extends CrudRepository<UserDetails, Long> {

    UserDetails findByEmail(String email);

}

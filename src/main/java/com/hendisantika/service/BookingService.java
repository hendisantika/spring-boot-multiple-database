package com.hendisantika.service;

import com.hendisantika.model.Booking;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-database
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 06.36
 * To change this template use File | Settings | File Templates.
 */

public interface BookingService {

    List<Booking> findUserBookings(String emailId);
}

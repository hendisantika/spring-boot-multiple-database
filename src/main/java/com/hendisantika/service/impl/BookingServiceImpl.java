package com.hendisantika.service.impl;

import com.hendisantika.booking.dao.BookingDao;
import com.hendisantika.model.Booking;
import com.hendisantika.model.UserDetails;
import com.hendisantika.service.BookingService;
import com.hendisantika.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-database
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 06.37
 * To change this template use File | Settings | File Templates.
 */

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BookingDao bookingDao;

    public List<Booking> findUserBookings(String emailId) {
        UserDetails userdetails = userDao.findByEmail(emailId);
        List<Booking> bookings = bookingDao.findByCreatedBy(userdetails.getId());
        return bookings;
    }
}

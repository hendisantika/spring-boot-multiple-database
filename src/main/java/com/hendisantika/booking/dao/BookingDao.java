package com.hendisantika.booking.dao;

import com.hendisantika.model.Booking;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-database
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/17
 * Time: 06.40
 * To change this template use File | Settings | File Templates.
 */

public interface BookingDao extends CrudRepository<Booking, Long> {

    List<Booking> findByCreatedBy(Long userId);

}
